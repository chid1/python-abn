Release History
---------------

Unreleased
++++++++++

**Improvements**

 - Add GitLab continuous integration script to run tox tests.


0.3.7 (2016-08-03)
++++++++++++++++++

**Improvements**

 - Add test suite to `setup.py`. You can run with `python setup.py test`.


0.3.6 (2015-08-03)
++++++++++++++++++

**Bug fixes**

 - Enable tests for Python 2.7, 3.3 and 3.4.
